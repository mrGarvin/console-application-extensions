﻿using System;
using System.Linq;

namespace ConsoleApplicationExtensions.Utilities
{
    public static class StringUtilities
    {
        public static string[] ReplaceInAll(this string[] stringArray, string oldValue, string newValue)
        {
            if (stringArray == null)
                throw new ArgumentNullException($"{nameof(stringArray)} cannot be null.");
            if (oldValue == null)
                throw new ArgumentNullException($"{nameof(oldValue)} cannot be null.");
            if (newValue == null)
                throw new ArgumentNullException($"{nameof(newValue)} cannot be null.");

            string[] strArr = new string[stringArray.Length];
            for (int i = 0; i < stringArray.Length; i++)
            {
                strArr[i] = stringArray[i].Replace(oldValue, newValue);
            }

            return strArr;
        }

        public static int LongestStringCount(this string[] stringArray)
        {
            if (stringArray == null)
                throw new ArgumentNullException($"{nameof(stringArray)} cannot be null.");
            int longestCount = 0;
            foreach (string s in stringArray)
            {
                longestCount = s.Length > longestCount ? s.Length : longestCount;
            }

            return longestCount;
        }

        /// <summary>
        /// Reports the zero-based index of the first occurrence in this instance of any character in a specified array of Unicode characters.
        /// </summary>
        /// <returns>
        /// The zero-based index position of the first occurrence in this instance where any character in anyOf was found; -1 if no character in anyOf was found.
        /// </returns>
        /// <param name="str"></param>
        /// <param name="anyOf"></param>
        /// <param name="ignore"></param>
        /// <returns></returns>
        public static int IndexOfAny(this string str, char[] anyOf, char[] ignore)
        {
            foreach (char c in str)
            {
                if (c != '\\' && ignore.Contains(c))
                    return str.IndexOf(c);
            }

            return -1;
        }

        public static bool IsUppercase(this string str)
        {
            for (int i = 0; i < str.Length; i++)
            {
                if (char.IsLower(str, i))
                    return false;
            }
            return true;
        }

        public static bool IsLowerCase(this string str)
        {
            for (int i = 0; i < str.Length; i++)
            {
                if (char.IsUpper(str, i))
                    return false;
            }
            return true;
        }

        public static string UppercaseRange(this string str, int startIndex, int count)
        {
            if (startIndex < 0)
                throw new ArgumentOutOfRangeException($"{nameof(startIndex)} cannot be less than zero.");
            if (startIndex >= str.Length)
                throw new ArgumentOutOfRangeException($"{nameof(startIndex)} cannot be larger or equal to the lengt of {nameof(str)}.");
            if (count < 0)
                throw new ArgumentOutOfRangeException($"{nameof(count)} cannot be less than zero.");
            if (count >= str.Length)
                throw new ArgumentOutOfRangeException($"{nameof(count)} cannot be larger or equal to the lengt of {nameof(str)}.");

            string range = str.Substring(startIndex, count);
            str = str.Replace(range.ToUpper(), startIndex);
            return str;
        }

        public static string LowercaseRange(this string str, int startIndex, int count)
        {
            if (startIndex < 0)
                throw new ArgumentOutOfRangeException($"{nameof(startIndex)} cannot be less than zero.");
            if (startIndex >= str.Length)
                throw new ArgumentOutOfRangeException($"{nameof(startIndex)} cannot be larger or equal to the lengt of {nameof(str)}.");
            if (count < 0)
                throw new ArgumentOutOfRangeException($"{nameof(count)} cannot be less than zero.");
            if (count >= str.Length)
                throw new ArgumentOutOfRangeException($"{nameof(count)} cannot be larger or equal to the lengt of {nameof(str)}.");

            string range = str.Substring(startIndex, count);
            str = str.Replace(range.ToLower(), startIndex);
            return str;
        }

        public static string Replace(this string str, string value, int startIndex)
        {
            if (value == null)
                throw new ArgumentNullException($"{nameof(value)} cannot be null.");
            if (startIndex < 0)
                throw new ArgumentOutOfRangeException($"{nameof(startIndex)} cannot be less than zero.");
            if (startIndex >= str.Length)
                throw new ArgumentOutOfRangeException($"{nameof(startIndex)} cannot be larger or equal to the lengt of {nameof(str)}.");

            str = str.Remove(startIndex, value.Length);
            str = str.Insert(startIndex, value);
            return str;
        }
    }
}