﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleApplicationExtensions.Utilities;

namespace ConsoleApplicationExtensions
{
    // Created by Shahram Pourmakhdomi
    // Greatly improved by Martin Lindstedt
    public abstract class ConsoleExtension
    {
        protected struct UIntInputRange
        {
            private const uint _minValue = uint.MinValue;
            private const uint _maxValue = uint.MaxValue;
            public uint MinValue { get; }
            public uint MaxValue { get; }

            public UIntInputRange(uint minValue = _minValue, uint maxValue = _maxValue)
            {
                if (minValue > maxValue)
                {
                    MinValue = _minValue;
                    MaxValue = _maxValue;
                }
                else
                {
                    MinValue = minValue;
                    MaxValue = maxValue;
                }
            }
        }

        protected struct IntInputRange
        {
            private const int _minValue = int.MinValue;
            private const int _maxValue = int.MaxValue;
            public int MinValue { get; }
            public int MaxValue { get; }

            public IntInputRange(int minValue = _minValue, int maxValue = _maxValue)
            {
                if (minValue > maxValue)
                {
                    MinValue = _minValue;
                    MaxValue = _maxValue;
                }
                else
                {
                    MinValue = minValue;
                    MaxValue = maxValue;
                }
            }
        }

        protected struct DoubleInputRange
        {
            private const double _minValue = double.MinValue;
            private const double _maxValue = double.MaxValue - 1;
            public double MinValue { get; }
            public double MaxValue { get; }

            public DoubleInputRange(double minValue = _minValue, double maxValue = _maxValue)
            {
                if (minValue > maxValue)
                {
                    MinValue = _minValue;
                    MaxValue = _maxValue;
                }
                else
                {
                    MinValue = minValue;
                    MaxValue = maxValue;
                }
            }
        }

        protected struct DateTimeInputRange
        {
            public DateTime MinDateTime { get; }
            public DateTime MaxDateTime { get; }

            public DateTimeInputRange(DateTime minDateTime, DateTime maxDateTime)
            {
                if (minDateTime.Date > maxDateTime.Date)
                {
                    MinDateTime = DateTime.MinValue;
                    MaxDateTime = DateTime.MaxValue;
                }
                else
                {
                    MinDateTime = minDateTime;
                    MaxDateTime = maxDateTime;
                }
            }
        }

        protected struct OptionsStringArray
        {
            public string[] Options { get; }
            public bool OptionsStartAtZero { get; }

            public OptionsStringArray(string[] options, bool optionsStartAtZero = false)
            {
                if (options == null)
                    throw new ArgumentNullException($"{nameof(options)} cannot be null.");
                if (options.Length == 0)
                    throw new ArgumentException($"{nameof(options)} must contain at least 1 option.");

                Options = options;
                OptionsStartAtZero = optionsStartAtZero;
            }
        }

        protected struct OptionsDictionary
        {
            public Dictionary<ConsoleKey, string> Options { get; }
            public bool NumpadKeysAreUnique { get; }

            public OptionsDictionary(Dictionary<ConsoleKey, string> options, bool numpadKeysAreUnique = false)
            {
                if (options == null)
                    throw new ArgumentNullException($"{nameof(options)} cannot be null.");
                if (options.Count == 0)
                    throw new ArgumentException($"{nameof(options)} must contain at least 1 option.");
                if (options.ContainsKey(ConsoleKey.Escape))
                    throw new ArgumentException(
                        $"{nameof(options)} cannot contain 'ConsoleKey.Escape' because it is reserved as the abort key.");
                if (!numpadKeysAreUnique)
                {
                    List<int> listOfInts =
                    (from keyValuePair in options
                     where keyValuePair.Key.IsNumberKey()
                     select keyValuePair.Key.ToInt()).ToList();
                    if (listOfInts.Count != listOfInts.Distinct().Count())
                        throw new ArgumentException(
                            $"If {nameof(numpadKeysAreUnique)} is false, {nameof(options)} cannot contain a digit key and a numpad key with the same value.");
                }

                Options = options;
                NumpadKeysAreUnique = numpadKeysAreUnique;
            }
        }

        protected enum Number
        {
            Zero,
            One,
            Two,
            Three,
            Four,
            Five,
            Six,
            Seven,
            Eight,
            Nine,
            Ten
        }

        private ConsoleKey[] DigitKeys => new[]
        {
            ConsoleKey.D0, ConsoleKey.D1, ConsoleKey.D2, ConsoleKey.D3, ConsoleKey.D4,
            ConsoleKey.D5, ConsoleKey.D6, ConsoleKey.D7, ConsoleKey.D8, ConsoleKey.D9
        };

        private ConsoleKey[] NumPadKeys => new[]
        {
            ConsoleKey.NumPad0, ConsoleKey.NumPad1, ConsoleKey.NumPad2, ConsoleKey.NumPad3, ConsoleKey.NumPad4,
            ConsoleKey.NumPad5, ConsoleKey.NumPad6, ConsoleKey.NumPad7, ConsoleKey.NumPad8, ConsoleKey.NumPad9,
        };

        private const int DefaultMaxInputAttempts = 5;

        private bool _keepRunning;
        private readonly int _borderLength;
        private readonly Dictionary<ConsoleKey, Action> _commandMapper;
        private readonly Dictionary<ConsoleKey, string> _commandLabelMapper;
        private string _programName;
        protected string ProgramName
        {
            get { return _programName; }
            set
            {
                if (_programName is null)
                    _programName = value;
            }
        }

        protected ConsoleExtension()
        {
            Console.ForegroundColor = ConsoleColor.White;
            _commandMapper = new Dictionary<ConsoleKey, Action>();
            _commandLabelMapper = new Dictionary<ConsoleKey, string>();
            Initialize();
            _borderLength = GetMenuBorderLength();
            RunLoop();
        }

        protected abstract void Initialize();

        protected void RunLoop()
        {
            if (!_commandMapper.Any())
            {
                PrintError("No commands found!\nShutting down...");
                return;
            }
            _keepRunning = true;
            bool printMenu = true;
            do
            {
                ConsoleKey command;
                if (printMenu)
                {
                    PrintMenu();
                    Console.Write("Enter one of the commands above: ");
                    command = Console.ReadKey(true).Key;
                }
                else
                    command = Console.ReadKey(true).Key;
                if (!_commandMapper.ContainsKey(command))
                {
                    printMenu = false;
                    continue;
                }
                Console.Clear();
                ExecuteCommand(command);
                printMenu = true;
            } while (_keepRunning);
        }

        protected void EndLoop()
        {
            _keepRunning = false;
        }

        protected void ExecuteCommand(ConsoleKey command)
        {
            var action = _commandMapper[command];
            action();
        }

        protected void AddCommand(ConsoleKey command, string commandLabel, Action commandAction)
        {
            _commandLabelMapper[command] = commandLabel;
            _commandMapper[command] = commandAction;
        }

        private void PrintMenu()
        {
            if (!_commandLabelMapper.Any())
                return;
            Console.Clear();
            PrintProgramName();

            PrintBorder(_borderLength);
            foreach (var command in _commandLabelMapper.Keys)
            {
                var label = _commandLabelMapper[command];
                Console.WriteLine($"[{Convert.ToChar(command)}] {label}");
            }
            PrintBorder(_borderLength);
        }

        private void PrintProgramName()
        {
            if (ProgramName is null)
                return;
            PrintBorder(ProgramName.Length);
            Print(ProgramName, false, ConsoleColor.DarkGreen);
            PrintBorder(ProgramName.Length);
            Console.WriteLine();
        }

        private void PrintBorder(int length)
        {
            for (int i = 0; i < length; i++)
            {
                Console.Write("=");
            }
            Console.WriteLine();
        }

        private int GetMenuBorderLength()
        {
            int borderLength = 0;
            foreach (var command in _commandLabelMapper.Keys)
            {
                borderLength = ((_commandLabelMapper[command].Length + 4) > borderLength) ? _commandLabelMapper[command].Length + 4 : borderLength;
            }
            return borderLength;
        }

        protected string[] GetCommandLabels()
        {
            string[] commandLabels = new string[_commandLabelMapper.Count];
            int counter = 0;
            foreach (KeyValuePair<ConsoleKey, string> keyValuePair in _commandLabelMapper)
            {
                commandLabels[counter] = keyValuePair.Value;
                counter++;
            }
            return commandLabels;
        }

        protected string GetLabelOf(ConsoleKey command)
        {
            _commandLabelMapper.TryGetValue(command, out string commandLabel);
            return commandLabel;
        }

        /// <summary>
        /// Get uint from input.
        /// </summary>
        /// <param name="prompt"></param>
        /// <param name="inputRange"></param>
        /// <param name="maxInputAttempts"></param>
        /// <returns>uint, null if maximum input attempts is reached.</returns>
        protected uint? AskForUInt(string prompt, UIntInputRange inputRange = new UIntInputRange(), int maxInputAttempts = DefaultMaxInputAttempts)
        {
            int counter = 0;
            string interval = inputRange.MinValue == inputRange.MaxValue ? "" : $" ({inputRange.MinValue} to {inputRange.MaxValue})";

            while (counter < maxInputAttempts)
            {
                uint? answer = InputService.GetUInt(prompt + interval);
                if (inputRange.MinValue <= answer || answer <= inputRange.MaxValue)
                    return answer;
                Console.WriteLine($"The value is outside the the set interval{interval}.");

                counter++;
            }

            PrintMaxInputAttemptsMessage(maxInputAttempts);
            return null;
        }

        /// <summary>
        /// Get int from input.
        /// </summary>
        /// <param name="prompt"></param>
        /// <param name="inputRange"></param>
        /// <param name="maxInputAttempts"></param>
        /// <returns>int, null if maximum input attempts is reached.</returns>
        protected int? AskForInt(string prompt, IntInputRange inputRange = new IntInputRange(), int maxInputAttempts = DefaultMaxInputAttempts)
        {
            int counter = 0;
            string interval = inputRange.MinValue == inputRange.MaxValue ? "" : $" ({inputRange.MinValue} to {inputRange.MaxValue})";

            while (counter < maxInputAttempts)
            {
                int? answer = InputService.GetInt(prompt + interval);
                if (inputRange.MinValue <= answer || answer <= inputRange.MaxValue)
                    return answer;
                Console.WriteLine($"The value is outside the the set interval{interval}.");

                counter++;
            }

            PrintMaxInputAttemptsMessage(maxInputAttempts);
            return null;
        }

        protected int? AskForInt(string prompt, OptionsStringArray options, IntInputRange inputRange = new IntInputRange(), int maxInputAttempts = DefaultMaxInputAttempts)
        {
            string option = "[{0}]";
            if (options.Options.Length < 10)
                option += " ";
            else if (options.Options.Length < 100)
                option += "  ";
            else
                option += "   ";
            option += "{1}";
            int startValue = options.OptionsStartAtZero ? 0 : 1;
            for (int i = 0; i < options.Options.Length; i++)
            {
                Console.WriteLine(option, i + startValue, options.Options[i]);
            }
            return AskForInt(prompt, new IntInputRange(inputRange.MinValue, inputRange.MaxValue), maxInputAttempts);
        }

        /// <summary>
        /// Get double from input.
        /// </summary>
        /// <param name="prompt"></param>
        /// <param name="inputRange"></param>
        /// <param name="maxInputAttempts"></param>
        /// <returns>dobule, null if maximum input attempts is reached.</returns>
        protected double? AskForDouble(string prompt, DoubleInputRange inputRange = new DoubleInputRange(), int maxInputAttempts = DefaultMaxInputAttempts)
        {
            int counter = 0;
            string interval = Math.Abs(inputRange.MinValue - inputRange.MaxValue) < 5 ? "" : $" ({inputRange.MinValue} to {inputRange.MaxValue})";

            while (counter < maxInputAttempts)
            {
                double? answer = InputService.GetDouble(prompt + interval);
                if (inputRange.MinValue <= answer || answer <= inputRange.MaxValue)
                    return answer;
                Console.WriteLine($"The value is outside the the set interval{interval}.");

                counter++;
            }

            PrintMaxInputAttemptsMessage(maxInputAttempts);
            return null;
        }

        /// <summary>
        /// Get DateTime from input.
        /// </summary>
        /// <param name="prompt"></param>
        /// <param name="inputRange"></param>
        /// <param name="dateFormat"></param>
        /// <param name="maxInputAttempts"></param>
        /// <returns>DateTime, null if maximum input attempts is reached.</returns>
        protected DateTime? AskForDate(string prompt, DateTimeInputRange inputRange = new DateTimeInputRange(), string dateFormat = "", int maxInputAttempts = DefaultMaxInputAttempts)
        {
            int counter = 0;
            string interval = inputRange.MinDateTime.Date == inputRange.MaxDateTime.Date ? "" : $" ({inputRange.MinDateTime.ToShortDateString()} to {inputRange.MaxDateTime.ToShortDateString()})";
            if (!InputService.ValidDateFormats.Contains(dateFormat))
                dateFormat = InputService.DefaultDateFormat;

            while (counter < maxInputAttempts)
            {
                DateTime? date = InputService.GetDate(prompt + interval, dateFormat);
                if (date is null)
                    return null;
                if (inputRange.MinDateTime.Date <= date.Value.Date && date.Value.Date <= inputRange.MaxDateTime.Date)
                    return date;
                counter++;
            }

            PrintMaxInputAttemptsMessage(maxInputAttempts);
            return null;
        }

        /// <summary>
        /// Get string from input
        /// </summary>
        /// <param name="prompt"></param>
        /// <returns>String.</returns>
        protected string AskForString(string prompt)
        {
            return InputService.ReadLine(prompt);
        }

        /// <summary>
        /// Get bool from input.
        /// </summary>
        /// <param name="prompt"></param>
        /// <param name="enterAsDefault"></param>
        /// <returns>bool, null if maximum input attempts is reached.</returns>
        protected bool? AskForBool(string prompt, bool enterAsDefault = true)
        {
            return InputService.GetBool(prompt, enterAsDefault);
        }

        /// <summary>
        /// Get ConsoleKey from input.
        /// </summary>
        /// <param name="prompt"></param>
        /// <returns>ConsoleKey, null if maximum input attempts is reached.</returns>
        protected ConsoleKey AskForConsoleKey(string prompt, bool interceptKey = false)
        {
            Console.Write($"{prompt}: ");

            ConsoleKey answer = Console.ReadKey(interceptKey).Key;
            Console.WriteLine();
            return answer;
        }

        /// <summary>
        /// Get ConsoleKey from input.
        /// </summary>
        /// <param name="prompt"></param>
        /// <param name="options"></param>
        /// <param name="maxInputAttempts"></param>
        /// <returns>ConsoleKey, null if maximum input attempts is reached.</returns>
        protected ConsoleKey? AskForConsoleKey(string prompt, OptionsDictionary options, int maxInputAttempts = DefaultMaxInputAttempts)
        {
            int counter = 0;
            Console.WriteLine(prompt);
            foreach (var kvp in options.Options)
            {
                Console.Write(!options.NumpadKeysAreUnique
                    ? $"[{kvp.Key.ToInt()}]"
                    : $"[{(kvp.Key.IsDigitKey() ? kvp.Key.ToInt().ToString() : kvp.Key.ToString())}]");
                Console.WriteLine($" {kvp.Value}");
            }
            Console.WriteLine("[Escape] Abort");
            while (counter < maxInputAttempts)
            {
                Console.Write("Enter a key: ");
                ConsoleKey answer = Console.ReadKey().Key;
                Console.WriteLine();
                if (answer == ConsoleKey.Escape)
                    return null;
                if (!options.NumpadKeysAreUnique)
                    answer = answer.ToDigitKey();
                if (options.Options.ContainsKey(answer))
                    return answer;

                Console.WriteLine("Not a valid key.");
                counter++;
            }

            PrintMaxInputAttemptsMessage(maxInputAttempts);
            return null;
        }

        protected Dictionary<ConsoleKey, string> CreateOptionsDictionary(ConsoleKey[] keys, string[] options)
        {
            if (keys == null)
                throw new ArgumentNullException($"{nameof(keys)} cannot be null.");
            if (keys.Length == 0)
                throw new ArgumentException($"{nameof(keys)} must contain at least 1 key.");
            if (options == null)
                throw new ArgumentNullException($"{nameof(options)} cannot be null.");
            if (options.Length == 0)
                throw new ArgumentException($"{nameof(options)} must contain at least 1 option.");
            if (keys.Length != options.Length)
                throw new ArgumentException($"{nameof(keys)} and {nameof(options)} must contain the same amount of objects.");
            if (keys.Length != keys.Distinct().Count())
                throw new ArgumentException("All keys must be unique.");

            Dictionary<ConsoleKey, string> optionsDictionary = new Dictionary<ConsoleKey, string>();
            for (int i = 0; i < keys.Length; i++)
            {
                optionsDictionary.Add(keys[i], options[i]);
            }

            return optionsDictionary;
        }

        protected ConsoleKey[] GetDigitKeys(Number upToNumber, bool startAtZero = true)
        {
            if (!startAtZero && upToNumber == Number.Zero)
                throw new ArgumentException($"If {nameof(upToNumber)} is Zero, {nameof(startAtZero)} cannot be false.");
            List<ConsoleKey> keys = new List<ConsoleKey>();
            int startIndex = startAtZero ? 0 : 1;
            for (int i = startIndex; i < (int)upToNumber + startIndex; i++)
            {
                keys.Add(DigitKeys[i]);
            }

            return keys.ToArray();
        }

        protected ConsoleKey[] GetNumPadKeys(Number upToNumber, bool startAtZero = true)
        {
            if (!startAtZero && upToNumber == Number.Zero)
                throw new ArgumentException($"If {nameof(upToNumber)} is Zero, {nameof(startAtZero)} cannot be false.");
            List<ConsoleKey> keys = new List<ConsoleKey>();
            int startIndex = startAtZero ? 0 : 1;
            for (int i = startIndex; i < (int)upToNumber + startIndex; i++)
            {
                keys.Add(NumPadKeys[i]);
            }

            return keys.ToArray();
        }

        /// <summary>
        /// Returns false if maximum input attempts is reached.
        /// </summary>
        /// <param name="prompt"></param>
        /// <param name="enterAsDefault"></param>
        /// <param name="maxInputAttempts"></param>
        /// <returns></returns>
        protected bool Confirm(string prompt, bool enterAsDefault = true, int maxInputAttempts = DefaultMaxInputAttempts)
        {
            int counter = 0;

            while (counter < maxInputAttempts)
            {
                string promtEnding = enterAsDefault ? "(Y/n)" : "(y/N)";
                Console.Write($"{prompt} {promtEnding}:");
                ConsoleKey inputKey = Console.ReadKey(true).Key;
                Console.WriteLine();
                switch (inputKey)
                {
                    case ConsoleKey.Y:
                        return true;
                    case ConsoleKey.N:
                        return false;
                    case ConsoleKey.Enter:
                        return enterAsDefault;
                }
                counter++;
            }

            PrintMaxInputAttemptsMessage(maxInputAttempts);
            return false;
        }

        /// <summary>
        /// Writes to console: reason (if not null), exception (if not null) and "Returning to main menu.". Option: Print with error colors
        /// </summary>
        /// <param name="reason"></param>
        /// <param name="exception"></param>
        /// <param name="printErrorColors"></param>
        protected void PrintReturningToMainMenu(string reason = null, Exception exception = null, bool printErrorColors = true)
        {
            if (printErrorColors)
                PrintError(reason, exception, false);
            else
            {
                if (!string.IsNullOrWhiteSpace(reason))
                    Console.WriteLine(reason);
                if (exception != null)
                    Console.WriteLine(exception);
                Console.WriteLine();
            }
            Console.WriteLine("Returning to main menu.");
            Console.ReadKey(false);
        }

        protected void Print(string prompt,
                             bool waitForKeyPress = true,
                             ConsoleColor backgroundColor = ConsoleColor.Black,
                             ConsoleColor foregroundColor = ConsoleColor.White)
        {
            Console.BackgroundColor = backgroundColor;
            Console.ForegroundColor = foregroundColor;
            Console.WriteLine(prompt);
            ResetConsoleColors();
            if (waitForKeyPress)
                Console.ReadKey(false);
        }

        protected void PrintWarning(string message, bool waitForKeyPress = true)
        {
            Console.BackgroundColor = ConsoleColor.DarkYellow;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(message);
            ResetConsoleColors();
            Console.WriteLine();
            if (waitForKeyPress)
                Console.ReadKey(false);
        }

        protected void PrintError(string message, Exception exception = null, bool waitForKeyPress = true)
        {
            Console.BackgroundColor = ConsoleColor.DarkRed;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(message);
            if (exception != null)
            {
                Console.BackgroundColor = ConsoleColor.DarkYellow;
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(exception);
            }
            ResetConsoleColors();
            Console.WriteLine();
            if (waitForKeyPress)
                Console.ReadKey(false);
        }

        protected void PrintMethodName(string methodName)
        {
            if (methodName == null)
                throw new ArgumentNullException($"{nameof(methodName)} cannot be null.");
            if (methodName.Length == 0)
                throw new ArgumentException($"{nameof(methodName)} cannot be an empty string.");

            PrintBorder(methodName.Length);
            Print(methodName, false, ConsoleColor.DarkGreen);
            PrintBorder(methodName.Length);
            Console.WriteLine();
        }

        protected void PrintAbout(string creator, Dictionary<string, string[]> labelsAndDescriptions)
        {
            Print($"Created by: {creator}", false, ConsoleColor.DarkBlue);
            Console.WriteLine();
            foreach (KeyValuePair<string, string[]> keyValuePair in labelsAndDescriptions)
            {
                Print($"* {keyValuePair.Key}", false, ConsoleColor.DarkYellow);
                foreach (string s in keyValuePair.Value)
                {
                    Console.WriteLine($"    {s}");
                }
            }
            PrintReturningToMainMenu();
        }

        protected void ResetConsoleColors()
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
        }

        private void PrintMaxInputAttemptsMessage(int maxInputAttempts)
        {
            Console.WriteLine($"Maximum input attempts ({maxInputAttempts}) reached.");
        }
    }
}
